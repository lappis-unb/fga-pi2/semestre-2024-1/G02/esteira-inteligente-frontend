import styled from "styled-components";

export const ContainerHome = styled.div`
  display: grid;
  width: 100%;
  height: auto;
  gap: 20px;
  background-color: #FFFAFA;
`
export const Container = styled.div`
  display: flex;
  flex-direction: row;
  margin-inline: 50px;
  flex: 1;
  align-items: center
`
export const Image = styled.img`
  width: 561px;
  height: 369px;
  background-size: cover;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;

export const Text = styled.div`
  text-align: justify;
  font-family: 'Urbanist';
  justify-content: center;
  flex: 1;
  padding: 20px;
`

export const Card = styled.div`
  width: 100%;
  height: auto;
  background-image: url('data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 100"><path d="M0 10 Q40 30 135 5 T300 100 H0 Z" fill="rgb(28,87,57,1)" /></svg>');
  background-size: cover;
  padding-top: 100px;
  margin-top: 50px;
  padding-bottom: 200px;
`;

export const Carrossel = styled.div`
  display: flex;
  flex-direction: column;
  overflow: hidden;
  width: 100px;
  height: auto;
  border: 2px solid #FFFFFF;
  border-radius: 8px;
  gap: 10px;
  padding: 10px;
  margin-top: 10vh;
`

export const RodapeEstendido = styled.div`
  background-color: #1C5739;
  width: 100%;
  height: auto;
  text-align: center;
  margin-top: -20px;
  padding: 1px;
  
  color: #fff;
  font-size: 16px;
`;