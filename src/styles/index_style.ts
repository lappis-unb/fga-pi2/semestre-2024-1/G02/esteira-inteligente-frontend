import styled from "styled-components";

type PageProps = {
  ativo?: boolean;
};

export const Container = styled.div`
  display: grid;

  .sucesso {
    background: #4BB543;
    color: #fff;
  }

  .erro {
    background: #FF3333;
    color: #fff;
  }

  .aviso {
    background: #EED202;
    color: #000;
  }

  & > :nth-last-child(2) {
    margin-bottom: 62px;
  }
`;

export const Title = styled.span`
  margin: auto;
  font-size: 32px;
  font-weight: 700;
  color: #1C5739;
  margin-top: 20px;
`;

export const KitContainer = styled.div`
  display: flex;
  align-items: center;
  margin: auto;
  margin-top: 20px;
  gap: 25px;
  width: 80%;

  @media only screen and (max-width: 1024px) {
    display: grid;
    justify-content: center;

    h1 {
      text-align: center;
    }

    img {
      justify-self: center;
    }
  }
`;

export const KitImage = styled.img`
  border: 2px solid #1C5739;
  border-radius: 8px;
`;

export const KitText = styled.div`
  display: grid;
  font-weight: 700;
  
  h1 {
    color: #1C5739;
    font-size: 24px;
  }
`;

export const KitButton = styled.button`
  margin-left: auto;
  justify-self: end;
  border-radius: 5px;
  background: #1C5739;
  border: 2px solid #1C5739;
  padding: 14px 70px;
  margin-top: 60px;
  cursor: pointer;
  box-shadow: 0 6px 4px rgba(0, 0, 0, .25);

  color: #fff;
  text-align: center;
  font-family: Arial;
  font-size: 20px;
  font-style: normal;
  font-weight: 500;
  line-height: normal;

  transition: ease-in-out 200ms;

  :hover {
    color: #1C5739;
    background: white;
    box-shadow: 0px 0px 10px #1C5739;
  }

  @media only screen and (max-width: 1024px) {
    justify-self: center;
    margin-left: 0;
    width: 100%;
  }
`;

export const ReturnBox = styled.div`
  display: grid;
  align-items: center;
  justify-content: center;
  text-align: center;
  width: 60%;
  margin: auto;
  margin-top: 60px;
  padding: 40px;
  gap: 20px;
  box-shadow: 0px 4px 4px rgba(0,0,0,.25);
  border-radius: 16px;
  position: relative;

  font-size: 48px;
  font-weight: 700;

  img {
    margin: auto;
  }
`;

export const ReturnLink = styled.a`
  margin: auto;
  margin-top: 40px;
  width: 235px;
  align-self: center;
  justify-self: center;
  display: flex;
  align-items: center;
  justify-content: center;

  button {
    margin: 0;
  }
`;

export const Tabela = styled.table`
  font-family: "Inter", sans-serif;
  min-width: 1000px;
  width: 80%;
  height: 400px;
  max-height: 500px;
  overflow: auto;
  margin: 0 auto;
  clear: both;
  border-collapse: collapse;
  border-spacing: 0;
  font-size: 0.9em;
  border-bottom: 1px solid #1C5739;
  color: #000;
  font-weight: 500;
  text-align: center;
  display: block;

  .cabeca {
    background-color: #fff !important;
    width: 100%;
    border-bottom: 1px solid #1C5739 !important;
    font-weight: 700;
    font-size: 16px;
  }


  tr{
    height: 30px;
  }

  td{
    width: 300px;
  }

  tr:nth-child(odd){
    background-color: #F9F9F9;
  }
`;

export const SemHistMessagem = styled.div`
  font-family: "Inter", sans-serif;
  min-width: 1000px;
  width: 100%;
  height: 500px;
  max-height: 600px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 42px;
  font-weight: 700;
  color: #1C5739;
`;

export const PagContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: end;
  text-align: center;
  gap: 10px;
  transition: ease-in-out 200ms;
  margin-top: 20px;
  margin-right: 10%;

  span {
    color: #1C5739;
    font-size: 16px;
    margin-left: 10px;
    margin-right: 10px;
    font-weight: 700;
  }
`;

export const PagButton = styled.button<PageProps>`
  display: flex;
  width: 40px;
  height: 40px;
  justify-content: center;
  align-items: center;
  background: ${props => props.ativo ? "#1C5739" : "#FFF"};
  border: none;
  color: ${props => props.ativo ? "#FFF" : "#1C5739"};
  cursor: pointer;
  filter: drop-shadow(0 4px 4px rgba(0,0,0,.25));
  z-index: 1;
`;

export const NavButton = styled.button`
  display: flex;
  width: 40px;
  height: 40px;
  justify-content: center;
  align-items: center;
  background: #fff;
  border: none;
  color: #1C5739;
  cursor: pointer;
  font-size: 16px;
  font-weight: 700;
`;

export const QuantidadeHist = styled.span`
  text-align: center;
  color: #1C5739;
  font-size: 16px;
  font-weight: 500;
  align-self: center;
`;

export const CountArea = styled.select`
  height: 40px;
  width: 66px;
  align-self: end;
  justify-self: end;
  background: #fff;
  display: flex;
  padding: 10px;
  border: #1C5739 2px solid;
  border-radius: 6px;
  color: #1C5739;
  cursor: pointer;
  font-size: 16px;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  margin-left: 50px;

  :focus {
    outline: none;
  }
`;
