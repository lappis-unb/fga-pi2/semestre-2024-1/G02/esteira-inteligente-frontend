import { Container } from "./style";

export const Footer = () => {

    return (
        <Container>
            <span>Universidade de Brasília</span>
        </Container>
    );
};
