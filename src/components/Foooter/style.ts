import styled from 'styled-components';

export const Container = styled.div`
  background-color: #1C5739;
  display: grid;
  width: 100%;
  height: 42px;
  align-items: center;
  justify-content: center;
  margin-top: auto;
  position: fixed;
  bottom: 0;
  
  color: #fff;
  font-size: 16px;
  z-index: 2;
`;