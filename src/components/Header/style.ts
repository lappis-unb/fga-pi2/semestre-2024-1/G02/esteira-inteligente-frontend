import styled from 'styled-components';

export const Container = styled.div`
  background-color: #1C5739;
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: flex-end;
  padding: 12px 16px;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
  position: relative;
  z-index: 1;
`;

export const LinkContainer = styled.div`
  display: flex;
  position: absolute;
  align-items: center;
  justify-self: center;
  left: 12px;
  gap: 10px;
`;

export const LinkA = styled.a`
  color: white;
  border: 1px solid #fff;
  padding: 10px 14px;
  border-radius: 8px;
  transition: 200ms ease-in-out;

  font-weight: 500;

  :hover  {
    background: #FFF;
    color: #1C5739;
  }
`;