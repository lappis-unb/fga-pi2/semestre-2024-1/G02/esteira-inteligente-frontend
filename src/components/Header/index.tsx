/* eslint-disable @next/next/no-img-element */
import { LogoutButton } from "../Logout";
import { CustomButton } from "../Logout/style";
import { Container, LinkA, LinkContainer } from "./style";
import { useEffect, useState } from "react";

interface HeaderProps {
  pag: string;
}

export const Header: React.FC<HeaderProps> = ({ pag }) => {
  const [isAuth, setIsAuth] = useState(false);

  useEffect(() => {
    if (localStorage.getItem("access_token") !== null) {
      setIsAuth(true);
    }
  }, [isAuth]);

  const acessarLogin = () => {
    window.location.href = "/login";
  };
  return (
    <Container>
      {/* <img src="./logo_header.png" alt="logo_header" width={60} height={55} /> */}
      {/* <TextContainer>{pag}</TextContainer> */}
      {isAuth && (
        <LinkContainer>
          <LinkA href="./">Kits</LinkA>
          <LinkA href="./historico">Historico</LinkA>
        </LinkContainer>
      )}
      {isAuth && <LogoutButton />}
      {!isAuth && <CustomButton onClick={acessarLogin}>LOGIN</CustomButton>}
    </Container>
  );
};
