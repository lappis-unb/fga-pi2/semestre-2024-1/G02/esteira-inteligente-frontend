export type ComponenteType = {
  id_componente: Number,
  nome: string,
  tipo: string,
  quantidade: Number
}

export type KitType = {
  id_kit: Number,
  kit_nome: string,
  componentes: ComponenteType[],
  quantidade_peca_total: Number,
  peso: string
}

export type TiposType = {
  id: Number,
  nome: string
}

export type HistoricoType = {
  id: Number,
  hora_producao: string,
  qtd_pecas: Number,
  id_kit: Number,
}

export type IAType = {
  classes_identificadas: string[],
  confianca: Number[],
  image: string
}