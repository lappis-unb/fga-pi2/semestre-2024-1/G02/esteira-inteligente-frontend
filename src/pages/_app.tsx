import { ThemeProvider } from "styled-components";
import { GlobalStyles } from "../styles/globals";
import type { AppProps } from "next/app";
import { theme } from "../styles/Theme";
import React from "react";

const App = ({ Component, pageProps }: AppProps): JSX.Element => {
  return (
    <ThemeProvider theme={theme}>
      <React.Fragment>
        <GlobalStyles />
        <Component {...pageProps} />
      </React.Fragment>
    </ThemeProvider>
  );
};

export default App;
