import { useEffect, useState } from "react";

import axios from "axios";
import { Header } from "../../components/Header";
import { Footer } from "../../components/Foooter";
import {
  Container,
  KitButton,
  KitContainer,
  KitImage,
  KitText,
  ReturnBox,
  ReturnLink,
  Title,
} from "../../styles/index_style";

export default function Sucesso() {
  const [message, setMessage] = useState("");
  const [imageIA, setImageIA] = useState<string | any>(null);
  const [peso, setPeso] = useState("")

  useEffect(() => {
    // Recuperando mensagens do localStorage
    const imageIALocal = localStorage.getItem("imageIA");

    if (imageIALocal) {
      setImageIA(`data:image/png;base64,${imageIALocal}`);
    }
  }, []);

  useEffect(() => {
    if (localStorage.getItem("access_token") === null) {
      window.location.href = "/home";
    } else {
      (async () => {
        try {
          const { data } = await axios.get(
            "http://192.168.43.29:8000/api/home/",
            {
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("access_token")}`,
              },
            }
          );
          setMessage(data.message);
        } catch (e) {
          console.log("not auth");
        }
      })();
    }
  }, []);

  useEffect(() => {
    
    (async () => {
      try {
        const { data } = await axios.get("http://192.168.43.29:8000/api/read-scale-data/");
        console.log(data.data)
        setPeso(data.data);
      } catch (e) {
        console.log("not auth");
      }
    })();
}, []);

  return (
    <>
      <Container>
        <Header pag="KITS" />
        <ReturnBox className="sucesso">
          <img src={imageIA} alt="Sucesso" width={340} height={340} style={{borderRadius: "16px", border: "solid #FFF 1px", boxShadow: "0px 4px 4px rgba(0,0,0,.25)"}} />
          <span>Massa: {peso} g</span>
          <span>KIT produzido com sucesso</span>
        </ReturnBox>
        <ReturnLink href="./">
          <KitButton>Voltar</KitButton>
        </ReturnLink>
        <Footer />
      </Container>
    </>
  );
}
