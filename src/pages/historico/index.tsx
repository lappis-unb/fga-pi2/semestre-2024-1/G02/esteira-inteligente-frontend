import { useEffect, useState } from "react";

import axios from "axios";
import { Header } from "../../components/Header";
import { Footer } from "../../components/Foooter";
import {
  Container,
  CountArea,
  KitButton,
  KitContainer,
  KitImage,
  KitText,
  NavButton,
  PagButton,
  PagContainer,
  QuantidadeHist,
  SemHistMessagem,
  Tabela,
  Title,
} from "../../styles/index_style";
import { HistoricoType, IAType, KitType } from "../../types/types";
import { ToastContainer, toast } from "react-toastify";

export default function Home() {
  const [historico, setHistorico] = useState<HistoricoType[]>([]);
  const [histAtualizado, sethistAtualizado] = useState(false);
  const [countHist, isCountHist] = useState(10);
  const [pagina, isPagina] = useState(0);
  const [countTicket, isCountTicket] = useState(10);
  const [count, isCount] = useState(false);

  useEffect(() => {
    if (localStorage.getItem("access_token") === null) {
      window.location.href = "/home";
    } else {
      (async () => {
        try {
          const { data } = await axios.get(
            "http://192.168.43.29:8000/api/home/",
            {
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("access_token")}`,
              },
            }
          );
        } catch (e) {
          console.log("not auth");
        }
      })();
    }
  }, []);

  useEffect(() => {
    const getKits = async () => {
      try {
        const response = await axios.get(
          "http://192.168.43.29:8000/api/historico-producao"
        );
        setHistorico(response.data);
        console.log(response.data);
        if (histAtualizado == false) sethistAtualizado(true);
      } catch (error) {
        console.error("Erro ao buscar os kits:", error);
      }
    };

    getKits();
  }, [histAtualizado]);

  const totalBotoes = Math.ceil(historico.length / countHist);

  const paginas = Array.from({ length: totalBotoes }, (_, index) => index + 1);

  const altera_pag_cont = async (e: { target: { value: any; }; }) =>{
    isCountHist(Number(e.target.value));
    isPagina(Number(0));
  }

  function formatarData(entrada: string): string {
    const data = new Date(entrada);
    
    const dia = data.getDate().toString().padStart(2, '0');
    const mes = (data.getMonth() + 1).toString().padStart(2, '0'); 
    const ano = data.getFullYear();
    const horas = data.getHours().toString().padStart(2, '0');
    const minutos = data.getMinutes().toString().padStart(2, '0');
    const segundos = data.getSeconds().toString().padStart(2, '0');
    
    return `${dia}/${mes}/${ano} ${horas}:${minutos}:${segundos}`;
  }

  return (
    <>
      <Container>
        <Header pag="Histórico" />
        <ToastContainer />
        <Title>HISTÓRICO</Title>

        {historico.length != 0 && 
        <div style={{display: "flex", gap: "10px", alignItems: "center", justifyContent: "flex-end", margin: "10px", width: "90%"}}>
          <QuantidadeHist>
              {(countHist * (pagina + 1)) - countHist + 1}
              {(countHist * (pagina + 1)) > historico.length && <> até {historico.length} de </>} 
              {(countHist * (pagina + 1)) <= historico.length && <> até {countHist * (pagina + 1)} de </>} 
              {historico.length} Kits Produzidos
          </QuantidadeHist>
          <CountArea 
            className={count? "aberto" : "fechado"} 
            onClick={() => isCount(!count)}
            value={countHist}
            onChange={altera_pag_cont}
          >
            <option value={"10"} onClick={() => isCountHist(10)}>10</option>
            <option value={"25"} onClick={() => isCountHist(25)}>25</option>
            <option value={"50"} onClick={() => isCountHist(50)}>50</option>
          </CountArea>
        </div>
        }

        {historico.length != 0 && 
            <Tabela role="grid">
              <thead>
                <tr className="cabeca">
                  <th tabIndex={0} rowSpan={1} colSpan={1}>ID</th>
                  <th tabIndex={0} rowSpan={1} colSpan={1}>Horário da Produção</th>
                  <th tabIndex={0} rowSpan={1} colSpan={1}>Quantidade de Peças</th>
                  <th tabIndex={0} rowSpan={1} colSpan={1}>Kit Produzido</th>
                </tr>
              </thead>
              <tbody>
                {historico.slice(pagina * countHist, countHist * (pagina + 1)).map(( hist,index) => 
                  <tr key={index} role="row">
                    <td>{hist.id.toString()}</td>
                    <td>{formatarData(hist.hora_producao)}</td>
                    <td>{hist.qtd_pecas.toString()} peças usadas</td>
                    <td>Kit {hist.id_kit.toString()}</td>
                  </tr>
                )}
              </tbody>
            </Tabela>
          }
          {historico.length === 0 && <SemHistMessagem>Histórico Vazio</SemHistMessagem>}
          
          <PagContainer>
            {pagina > 0 && 
              <NavButton onClick={() => isPagina(pagina -1)}>&lt;</NavButton>
            }
            {paginas.map((pagNu, index) => {
                if (pagNu >= pagina - 1 && pagNu <= pagina + 3) {
                    return (
                        <PagButton
                            onClick={() => isPagina(pagNu - 1)}
                            ativo={pagina + 1 === pagNu}
                            key={pagNu}
                        >
                            {pagNu}
                        </PagButton>
                    );
                } else if (pagNu === pagina + 4 || pagNu === pagina - 2) {
                    return <span key={`ellipsis${pagNu}`}>...</span>;
                } else if (pagNu === 1 || pagNu === paginas.length) {
                    return (
                        <PagButton
                            onClick={() => isPagina(pagNu - 1)}
                            ativo={pagina + 1 === pagNu}
                            key={pagNu}
                        >
                            {pagNu}
                        </PagButton>
                    );
                }
                return null;
            })}
            {pagina < totalBotoes - 1 && 
              <NavButton onClick={() => isPagina(pagina + 1)}>&gt;</NavButton>
            }
            {pagina == totalBotoes - 1 && 
              <NavButton style={{cursor: "default"}} />
            }
          </PagContainer>
        <Footer />
      </Container>
    </>
  );
}
