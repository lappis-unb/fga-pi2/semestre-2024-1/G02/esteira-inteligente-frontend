/* eslint-disable @next/next/no-img-element */
import { useEffect, useState } from "react";
import axios from "axios";
import {
  BorderContainer,
  Container,
  LoginButton,
  LoginIpunt,
} from "../../styles/login_style";

export default function Login() {
  const [isAuth, setIsAuth] = useState(false);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    if (localStorage.getItem("access_token") !== null) {
      setIsAuth(true);
    }
  }, [isAuth]);

  const submit = async (e: { preventDefault: () => void }) => {
    e.preventDefault();
    const user = {
      username: username,
      password: password,
    };
    const { data } = await axios.post(
      "http://192.168.43.29:8000/api/token/",
      user,
      {
        headers: { "Content-Type": "application/json" },
        withCredentials: true,
      }
    );

    localStorage.clear();
    localStorage.setItem("access_token", data.access);
    localStorage.setItem("refresh_token", data.refresh);
    axios.defaults.headers.common["Authorization"] = `Bearer ${data["access"]}`;
    window.location.href = "/";
  };

  return (
    <>
      <Container>
        <div>
          <span
            style={{
              marginLeft: "20px",
              textShadow: "0 6px 4px rgba(0, 0, 0, .25)",
            }}
          >
            Acesso Operador
          </span>
          <BorderContainer onSubmit={submit}>
            <LoginIpunt
              type="text"
              name="Usuario"
              placeholder="Operador"
              value={username}
              required
              onChange={(e) => setUsername(e.target.value)}
            />
            <LoginIpunt
              type="password"
              name="Senha"
              placeholder="Senha"
              value={password}
              required
              onChange={(e) => setPassword(e.target.value)}
            />
            <LoginButton type="submit">ENTRAR</LoginButton>
          </BorderContainer>
        </div>
      </Container>
    </>
  );
}
