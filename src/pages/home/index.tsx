import { useEffect, useState } from "react";

import axios from "axios";
import { Header } from "../../components/Header";
import { Card, Carrossel, Container, ContainerHome, Image, RodapeEstendido, Text } from "../../styles/home_style";
import { KitButton } from "../../styles/index_style";

export default function home() {
    const acessarPagina = () => {
        if (localStorage.getItem("access_token") === null) {
            window.location.href = "/login"
        }
        else window.location.href = "/"
    }

    interface ImagemCarrossel {
        src: string;
        titulo: string;
        texto: string;
    }
    
    const vetorImagens: ImagemCarrossel[] = [
        {src:"./esteira.png", titulo: "Separação de peças por esteira", texto: "Através de um motor de passos a esteira levará as peças certas para a montagem do kit."},
        {src:"./câmera.jpeg", titulo: "Sistema de câmeras", texto: "O sistema de reconhecimento de imagens por inteligência artificial irá verificar se as peças estão corretas."},
        {src:"./balança.png", titulo: "Controle de peso", texto: "A balança pesará as peças e, em conjunto com a IA, garantirá dupla checagem das peças do kit."}
    ]

    interface EstadoImagem {
        imagemAtiva: number;
    }  
    const [estado, setEstado] = useState<EstadoImagem>({ imagemAtiva: 0 });
    
    const imagemAtual = estado.imagemAtiva;
    
    return (
        <ContainerHome>
            <Header pag="PÁGINA INICIAL"/>
            
            <Container>
                {/* <Image src="./estrutura.jpeg"/> */}
                <iframe width="560" height="315" src="https://www.youtube.com/embed/ngYRNHUsAbY?si=xlIRyjWkupgmT6Uf" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerPolicy="strict-origin-when-cross-origin" allowFullScreen></iframe>
                <Text>
                    <p style={{fontSize: 40, fontWeight: 900, color: '#1C5739'}}>Mais controle na montagem de kits de fabricação</p>
                    <p style={{fontSize: 32, fontWeight: 300}}
                    >A esteira inteligente usa um sistema de reconhecimento de imagens para identificação de peças,
                     garantindo uma redução significativa no número de kits errados.
                    </p>
                    
                    <KitButton 
                        style={{marginLeft: 170, marginTop: 25, fontFamily: 'Urbanist'}}
                        onClick={acessarPagina}
                    >
                        Comece a Produzir
                    </KitButton>
                </Text>
            </Container>
            <Card style={{display: 'flex',flexGrow:1}}>
                <Container>
                    <Image style={{width: 406, height: 287, marginTop: "15vh"}} src={vetorImagens[imagemAtual].src}/>
                    <Text>
                        <p style={{fontSize: 40, fontWeight: 900, color: "#FFFFFF", marginTop: "15vh", marginRight: 25}}>{vetorImagens[imagemAtual].titulo}</p>
                        <p style={{fontSize: 32, fontWeight: 300, color: "#FFFFFF", marginRight: 25}}>{vetorImagens[imagemAtual].texto}</p>                    
                    </Text>

                    <Carrossel>
                        {vetorImagens.map((atual, index) => {
                            return (
                                <Image style={{width: "100%", height: 50}} onClick={() => {setEstado({imagemAtiva: index})}} src={atual.src} key={index}/>
                            )
                        })}
                    </Carrossel>
                </Container>
            </Card>
            <RodapeEstendido>
                <p>Universidade de Brasília</p>
                <p>Faculdade Gama-FGA</p>
                <p>Gama-DF / 2024</p>
            </RodapeEstendido>
        </ContainerHome>
    );
}