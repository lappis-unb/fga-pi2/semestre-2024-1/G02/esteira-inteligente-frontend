import { useEffect, useState } from "react";

import axios from "axios";
import { Header } from "../components/Header";
import { Footer } from "../components/Foooter";
import {
  Container,
  KitButton,
  KitContainer,
  KitImage,
  KitText,
  Title,
} from "../styles/index_style";
import { IAType, KitType } from "../types/types";
import { ToastContainer, toast } from "react-toastify";

export default function Home() {
  const [message, setMessage] = useState("");
  const [kits, setKits] = useState<KitType[]>([]);
  const [kitAtualizado, setKitAtualizado] = useState(false);
  const [iaRetorno, setIaRetorno] = useState<IAType[]>([]);
  const [iaRetornoTempoReal, setIaRetornoTempoReal] = useState<IAType[]>([]);

  useEffect(() => {
    if (localStorage.getItem("access_token") === null) {
      window.location.href = "/home";
    } else {
      (async () => {
        try {
          const { data } = await axios.get(
            "http://192.168.43.29:8000/api/home/",
            {
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("access_token")}`,
              },
            }
          );
          setMessage(data.message);
        } catch (e) {
          console.log("not auth");
        }
      })();
    }
  }, []);

  useEffect(() => {
    const getKits = async () => {
      try {
        const response = await axios.get(
          "http://192.168.43.29:8000/api/kits-agrupados"
        );
        setKits(response.data);
        console.log(kits);
        if (kitAtualizado == false) setKitAtualizado(true);
      } catch (error) {
        console.error("Erro ao buscar os kits:", error);
      }
    };

    getKits();
  }, [kitAtualizado]);

  function defineTexto(iaCount: any, KitCount: any, Component: String) {
    if (iaCount > KitCount) {
      let diff = iaCount - KitCount;
      let mensagemComponent = `${diff} ${Component} a mais`;
      localStorage.setItem(`mensagem${Component}`, mensagemComponent);
    } else if (iaCount < KitCount) {
      let diff = KitCount - iaCount;
      let mensagemComponent = `${diff} Component a menos`;
      localStorage.setItem(`mensagem${Component}`, mensagemComponent);
    }
  }

  function produzirKit(kit_fun: KitType) {
    console.log(kit_fun);
    axios.post(`http://192.168.43.29:8000/api/historico-producao/`, {
      qtd_pecas: kit_fun.quantidade_peca_total,
      id_kit: kit_fun.id_kit,
    });

    const getIA = async () => {
      try {
        const responseIa = await axios.get(
          "http://192.168.43.29:8000/api/consumir-servico-ia"
        );
        setIaRetorno(responseIa.data);
        console.log(responseIa.data);
        let { classes_identificadas, image } = responseIa.data;

        let countCirculo = classes_identificadas.filter(
          (objeto: string) => objeto === "circulo"
        ).length;
        let countQuadrado = classes_identificadas.filter(
          (objeto: string) => objeto === "quadrado"
        ).length;
        let countTriangulo = classes_identificadas.filter(
          (objeto: string) => objeto === "triangulo"
        ).length;

        let componenteCirculo = kit_fun.componentes.find(
          (componente) => componente.nome === "CIRCULO"
        );
        let componenteQuadrado = kit_fun.componentes.find(
          (componente) => componente.nome === "QUADRADO"
        );
        let componenteTriangulo = kit_fun.componentes.find(
          (componente) => componente.nome === "TRIANGULO"
        );

        localStorage.setItem("mensagemCirculo", "");
        localStorage.setItem("mensagemQuadrado", "");
        localStorage.setItem("mensagemTriangulo", "");
        localStorage.setItem("imageIA", image);

        // Ia que faz a contagem durante a execução da esteira
        const responseIaTempoReal = await axios.get(
          "http://192.168.43.29:8000/api/stop"
        );
        setIaRetornoTempoReal(responseIaTempoReal.data);

        if (componenteCirculo && componenteQuadrado && componenteTriangulo) {
          let quantidadeAmarelo = parseInt(
            String(componenteCirculo.quantidade),
            10
          );
          let quantidadeAzul = parseInt(
            String(componenteQuadrado.quantidade),
            10
          );
          let quantidadePreto = parseInt(
            String(componenteTriangulo.quantidade),
            10
          );

          // Verificando se countCirculo é maior ou menor que a quantidade de componenteCirculo
          if (countCirculo > quantidadeAmarelo) {
            let diffAmarelo = countCirculo - quantidadeAmarelo;
            let mensagemCirculo = `${diffAmarelo} circulo a mais`;
            localStorage.setItem("mensagemCirculo", mensagemCirculo);
          } else if (countCirculo < quantidadeAmarelo) {
            let diffAmarelo = quantidadeAmarelo - countCirculo;
            let mensagemCirculo = `${diffAmarelo} circulo a menos`;
            localStorage.setItem("mensagemCirculo", mensagemCirculo);
          }

          // Verificando se countQuadrado é maior ou menor que a quantidade de componenteQuadrado
          if (countQuadrado > quantidadeAzul) {
            let diffAzul = countQuadrado - quantidadeAzul;
            let mensagemQuadrado = `${diffAzul} quadrado a mais`;
            localStorage.setItem("mensagemQuadrado", mensagemQuadrado);
          } else if (countQuadrado < quantidadeAzul) {
            let diffAzul = quantidadeAzul - countQuadrado;
            let mensagemQuadrado = `${diffAzul} quadrado a menos`;
            localStorage.setItem("mensagemQuadrado", mensagemQuadrado);
          }

          // Verificando se countTriangulo é maior ou menor que a quantidade de componenteTriangulo
          if (countTriangulo > quantidadePreto) {
            let diffPreto = countTriangulo - quantidadePreto;
            let mensagemTriangulo = `${diffPreto} triângulo a mais`;
            localStorage.setItem("mensagemTriangulo", mensagemTriangulo);
          } else if (countTriangulo < quantidadePreto) {
            let diffPreto = quantidadePreto - countTriangulo;
            let mensagemTriangulo = `${diffPreto} triângulo a menos`;
            localStorage.setItem("mensagemTriangulo", mensagemTriangulo);
          }

          if (
            countCirculo === componenteCirculo.quantidade &&
            countQuadrado === componenteQuadrado.quantidade &&
            countTriangulo === componenteTriangulo.quantidade
          ) {
            window.location.href = "/sucesso";
          } else {
            window.location.href = "/erro";
          }
        } else {
          console.error("Componente não encontrado para todos os tipos.");
        }
      } catch (error) {
        console.error("Erro de comunicação com a IA:", error);
      }
    };

    const fetchAndCallGetIA = async (kitId: Number) => {
      try {
        const response = await axios.get(`http://192.168.43.29:8000/api/send-code/${kitId}/`);
        if (response.status === 200) {
          getIA();
        }
      } catch (error) {
        console.error("Erro na requisição:", error);
      }
    };

    fetchAndCallGetIA(kit_fun.id_kit);

  }

  return (
    <>
      <Container>
        <Header pag="KITS" />
        <ToastContainer />
        <Title>KITS</Title>
        {kits.map((index, count) => (
          <KitContainer key={index.kit_nome}>
            <KitImage
              src={`./kit${count+1}.jpg`}
              alt={`kit${count+1}`}
              width={240}
              height={162}
            />
            <KitText>
              <h1>{index.kit_nome}</h1>
              {index.componentes.map((index2, idx) => (
                <span key={idx}>
                  {index2.quantidade.toString()} X {index2.nome}{" "}
                  {index2.tipo}
                </span>
              ))}
              <span>{index.peso} g</span>
            </KitText>
            <KitButton onClick={() => produzirKit(index)}>Produzir</KitButton>
          </KitContainer>
        ))}
        <Footer />
      </Container>
    </>
  );
}
